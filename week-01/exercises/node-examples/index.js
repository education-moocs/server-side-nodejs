var rect = require('./rectangle');

function solveRect(length, berth) {
    console.log(`Solving for rectangle with length ${length} and berth ${berth}`);

    if (length <= 0 || berth <= 0) {
        console.log(`Rectangle dimensions should be greater than zero, but instead the length was ${length} and berth was ${berth}`);
    } else {
        console.log(`The area of the rectangle is ${rect.area(length, berth)}`);
        console.log(`The perimeter of the rectangle is ${rect.perimeter(length, berth)}`);
    }
}


solveRect(2, 4);
solveRect(3, 5);
solveRect(0, 5);
solveRect(-4, 7);